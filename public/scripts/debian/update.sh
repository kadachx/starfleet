#!/usr/bin/bash

trap 'echo >&2 "Error - exited with status $? at line $LINENO:"; 
         pr -tn $0 | tail -n+$((LINENO - 1)) | head -n7' ERR

if tty -s ; then
    tput reset
fi

maketemps(){
	country_log=$(mktemp)
	trap 'rm -f ${country_log}' 0 2 3 15
	ip_log=$(mktemp)
	trap 'rm -f ${ip_log}' 0 2 3 15
}

cleanup(){
	rm "${country_log}"
	rm "${ip_log}"
	log=
}

pressanykey(){
#	read -n1 -p "Press any key to continue."
	echo  "Press any key to continue."; read -n1 -s
}


maketemps
#-------------------------------------------------

checkReboot(){

	reboot=$(test -f /var/run/reboot-required; echo $?)

	if [ $reboot = 0 ]; then
		read -p "Computer requires a reboot. Do you want to reboot now? (y/n) " -n 1 -r
		printf " \n"
		if [[ $REPLY =~ ^[Yy]$ ]];then
			sudo reboot
		else
		    exec /bin/zsh
            tput reset && source "${HOME}"/.zshrc
		    exit 0
		fi
    else
        exec /bin/zsh
        tput reset && source "${HOME}"/.zshrc
        exit 0
	fi
}

#-------------------------------------------------

updateAPT(){
	sudo apt-get update
	sudo apt-get upgrade
	sudo apt-get dist-upgrade
	sudo dpkg-query -f '${binary:Package}\n' -W > /backup/apt/pkgs_installed.txt
}

#-------------------------------------------------

updateNALA(){
	sudo nala list --upgradeable
	sudo nala upgrade
	sudo dpkg-query -f '${binary:Package}\n' -W > /backup/apt/pkgs_installed.txt
}

#-------------------------------------------------

sudo flatpak update
printf 'Done updating flatpaks \n \n'
cinnamon-spice-updater --update-all
printf '\n'
#updateAPT
updateNALA
printf "Done with all Updates \n"

pressanykey

checkReboot

