#!/usr/bin/bash

trap 'echo >&2 "Error - exited with status $? at line $LINENO:"; 
         pr -tn $0 | tail -n+$((LINENO - 1)) | head -n7' ERR

if  tty -s ; then
    tput reset
fi

# --------------------------------------------------------

cleanup(){
	rm "${tmp_log}"
#	log=
}

maketemps(){
	tmp_log=$(mktemp)
	trap 'rm -f ${tmp_log}' 0 2 3 15
}

pause(){
   read -n1 -s -r -p "Press [Enter] key to continue..." < /dev/tty
}

pressanykey(){
#	read -n1 -p "Press any key to continue."
	echo  "Press any key to continue."; read -n1 -s
}

# --------------------------------------------------------
###maketemps

# --------------------------------------------------------

#!/bin/bash
SRC=$1
DEST=$2

SRC_GROUPS=$(id -Gn "${SRC}" | sed "s/ /,/g" | sed -r "s/\<"${SRC}"\>\b,?//g" | sed '$ s/.$//')
SRC_SHELL=$(awk -F : -v name="${SRC}" '(name == $1) { print $7 }' /etc/passwd)

sudo useradd --groups "${SRC_GROUPS}" --shell "${SRC_SHELL}" --create-home "${DEST}"
sudo passwd "${DEST}"


